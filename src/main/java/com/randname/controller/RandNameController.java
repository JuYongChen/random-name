package com.randname.controller;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RandNameController extends JFrame implements ActionListener {

    // 主函数入口
    public static void main(String[] args) {//throws是方法可能抛出异常的声明
        RandNameController r = new RandNameController();
        r.setVisible(true); // setVisible(boolean)方法是用来显示（true）/隐藏（false）GUI组件的
    }

    // 实例化
    private JPanel con = new JPanel();
    private JTextField input_text = new JTextField();
    private JButton c_Btn = new JButton("开始");

     public RandNameController() throws HeadlessException {
        init();
        addComponent();
    }

    public void addComponent() {    // 添加控件
        this.input_text.setPreferredSize(new Dimension(490, 80));//文本框大小
        this.c_Btn.setPreferredSize(new Dimension(100, 80));//按钮大小
        c_Btn.setFont(new Font("粗体", Font.BOLD, 30));    //按钮内文本格式

        Font font = new Font("楷体", Font.BOLD, 30);//文本框内文字格式
        this.input_text.setFont(font);

        this.c_Btn.setForeground(Color.RED);    //字体颜色
        con.add(input_text);
        con.add(c_Btn);

        c_Btn.addActionListener(new ActionListener() {//开始按钮的实现
            public void actionPerformed(ActionEvent e) {

                try {
                    input_text.setText(function());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        this.add(con, BorderLayout.CENTER);
    }

    public void actionPerformed(ActionEvent e) {

    }
    // 获取屏幕宽度
    public static final int SCREEN_W = Toolkit.getDefaultToolkit().getScreenSize().width;
    // 获取屏幕高度
    public static final int SCREEN_H = Toolkit.getDefaultToolkit().getScreenSize().height;

    public void init() {//初始化的方法
        this.setTitle("课堂随机点名");
        this.setSize(500, 300);

        this.setLayout(new BorderLayout());
        this.setResizable(false);
        this.setLocation((SCREEN_W - 500) / 2, (SCREEN_H - 300) / 2);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    public static String function() throws IOException {

        //名单路径
         File file = new File("D:\\aaa.txt");
//         File file = new File("D:\\1111.xlsx");
        if (file.exists()) {    // 判断文件是否存在
            BufferedReader r = new BufferedReader(new FileReader(file));//缓冲区读取内容，避免中文乱码
            ArrayList<String> list = new ArrayList<>();

            while (true) { // 读取文件的一行将其存入list数组中
                String s = r.readLine();
                if(s == null){
                    break;
                }
                list.add(s);
            }

            Random random = new Random();
            int i = random.nextInt(list.size());
            System.out.println(list.get(i));

            r.close();
            return ("被抽中的同学是：" + list.get(i));
        }
        else {
            return "文件不存在";
        }
    }
}

