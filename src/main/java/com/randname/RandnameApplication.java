package com.randname;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandnameApplication {

	public static void main(String[] args) {
		SpringApplication.run(RandnameApplication.class, args);
	}

}
