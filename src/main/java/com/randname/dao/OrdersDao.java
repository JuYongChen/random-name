package com.randname.dao;

import com.randname.entity.Orders;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrdersDao {

    List<Orders> getOrderList();

}
