package com.randname.entity;


import lombok.Data;

import java.util.Date;

@Data
public class Orders {
    private Integer orderNum;
    private Integer custId;
    private Date orderDate;
}
